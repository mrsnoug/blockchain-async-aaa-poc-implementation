# An Asynchronous AAA Blockchain-based Protocol for Configuring Information Systems

## This is a PoC application for reference only - it should not be used in production

### Prerequisites

- docker
- docker-compose
- Metamask browser extension
- solcjs (optional)


## Building and running

```bash
docker-compose up --build
```

The image building process might take a while. Furthermore - there will be quite a few of deprecation warnings -
those are mostly related to react-scripts.

If you wish to modify Smart Contracts and test your version within this environment, then you will also need to compile
and place them in proper directories. I did prepare a convenience script to do it:

```bash
./compile_contracts.sh
```

In order to run this script you will need solcjs. You can install it by running:

```bash
npm install -g solc
```

Additionally, please bear in mind that changing the Ethereum Events used will break the functionality on the Django server.
You will also need to modify the event listeners if those were changed.

If you wish to clear the backend database you will need to remove the content of "app" directory:

```bash
cd app
sudo rm -rf db
```

Additionally - please remove the postgres docker image as well and run docker-compose up --build again.

## Using the application

### Adding accounts to Metamask (do it only the first time you are running the application)

Once every image is running go to localhost:8000/admin, you should see the following view:
![Django admin login panel](./readme_images/adminPanelLogin.png)
Log in using admin as both username and password
(this admin section is to better understand what is going on in the background and to import proper accounts into Metamask).

After successful login you should see the following website, where you should clikc on the "Blockchain users" under "POC_BACKEND":
![Django admin panel](./readme_images/adminPanel.png)

In there, you should find three entries - two for Admin users and one for a regular user:
![Django admin blockchain users](./readme_images/adminPanelBlockchainUsers.png)

Clicking on each entry will reveal fields associated with those in the database:
![Django admin blockchain user data](./readme_images/adminPanelBlockchainUserData.png)

One of those fields is private key linked to that account.
This private key can be used to import the account into Metamask - obviously private keys should not be stored in the backend database
in the production environment.
Please copy that private key.
