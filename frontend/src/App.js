import React from "react";
import {BrowserRouter, Route, Switch} from "react-router-dom";
import HomePage from "./components/HomePage";
import CheckConfigPage from "./components/CheckConfigPage/CheckConfigPage";
import ChangeConfigPage from "./components/ChangeConfigPage/ChangeConfigPage";
import ConfigContext from "./components/contexts/ConfigContext";
import FetchConfigWrapper from "./components/FetchConfigWrapper";
import AuthorizeChangePage from "./components/AuthorizeChangePage/AuthorizeChangePage";
import CheckChangesPage from "./components/CheckChangesPage/CheckChangesPage";


function App() {
    const [stringData, setStringData] = React.useState("");
    const [booleanData, setBooleanData] = React.useState(null);
    const [numberData, setNumberData] = React.useState(null);
    const [websiteHeaderColor, setWebsiteHeaderColor] = React.useState("blue");

    return (
        <ConfigContext.Provider value={{
            booleanData: booleanData,
            setBooleanData: setBooleanData,
            stringData: stringData,
            setStringData: setStringData,
            numberData: numberData,
            setNumberData: setNumberData,
            websiteHeaderColor: websiteHeaderColor,
            setWebsiteHeaderColor: setWebsiteHeaderColor
        }}>
            <BrowserRouter>
                <Switch>
                    <FetchConfigWrapper exact path="/"><HomePage/></FetchConfigWrapper>
                    <FetchConfigWrapper exact path="/check_config"><CheckConfigPage/></FetchConfigWrapper>
                    <FetchConfigWrapper exact path="/change_config"><ChangeConfigPage/></FetchConfigWrapper>
                    <FetchConfigWrapper exact path="/check_changes"><CheckChangesPage/></FetchConfigWrapper>
                    <FetchConfigWrapper exact path="/authorize_change"><AuthorizeChangePage/></FetchConfigWrapper>
                </Switch>
            </BrowserRouter>
        </ConfigContext.Provider>
    );
}

export default App;
