import React, {useContext, useEffect} from "react";
import {Grid, Typography} from "@material-ui/core";
import ConfigContext from "../contexts/ConfigContext";
import axios from "axios";
import BACKEND_ADDRESS from "../../config";


const CheckConfigContent = () => {
    const {
        numberData,
        stringData,
        websiteHeaderColor,
        booleanData,
    } = useContext(ConfigContext);

    return (
        <Grid container direction="column">
            <Grid item>
                {numberData
                    ? (<Typography>Number data: {numberData.toString()}</Typography>) : (
                        <Typography>Number data: Fetching...</Typography>)}
            </Grid>
            <Grid item>
                {stringData
                    ? (<Typography>String data: {stringData}</Typography>) : (
                        <Typography>String data: Fetching...</Typography>)}
            </Grid>
            <Grid item>
                {booleanData
                    ? (<Typography>Boolean data: {booleanData}</Typography>) : (
                        <Typography>Boolean data: Fetching...</Typography>)}
            </Grid>
            <Grid item>
                {websiteHeaderColor
                    ? (<Typography>Website header is set to: {websiteHeaderColor}</Typography>) : (
                        <Typography>Fetching website header color</Typography>)}
            </Grid>
        </Grid>
    );
};

export default CheckConfigContent;
