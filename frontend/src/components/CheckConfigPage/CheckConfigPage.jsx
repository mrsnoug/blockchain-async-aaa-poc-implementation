import React, {useContext} from "react";
import {Grid, Typography} from "@material-ui/core";
import Header from "../Header";
import ConfigContext from "../contexts/ConfigContext";
import CheckConfigContent from "./CheckConfigContent";


const CheckConfigPage = () => {
    return (
        <Grid container direction="column">
            <Grid item>
                <Header pageTitle="Check Config"/>
            </Grid>
            <Grid item container style={{marginTop: "80px"}}>
                <Grid item xs={false} sm={3}/>
                <Grid item container xs={12} sm={6}>
                    <CheckConfigContent/>
                </Grid>
                <Grid item xs={false} sm={3}/>
            </Grid>
        </Grid>
    );
};

export default CheckConfigPage;
