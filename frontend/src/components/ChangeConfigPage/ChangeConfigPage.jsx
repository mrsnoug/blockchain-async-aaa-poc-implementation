import React from "react";
import {Grid, Typography} from "@material-ui/core";
import Header from "../Header";
import ChangeConfigContent from "./ChangeConfigContent";


const ChangeConfigPage = () => {
    return (
        <Grid container direction="column">
            <Grid item>
                <Header pageTitle="Change Config"/>
            </Grid>
            <Grid item container style={{marginTop: "80px"}}>
                <Grid item xs={false} sm={3}/>
                <Grid item container xs={12} sm={6}>
                    <ChangeConfigContent/>
                </Grid>
                <Grid item xs={false} sm={3}/>
            </Grid>
        </Grid>
    );
};

export default ChangeConfigPage;