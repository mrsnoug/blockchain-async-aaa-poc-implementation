import React, { useContext } from "react";
import {
    Button,
    Card,
    CardContent,
    Divider,
    FormControlLabel, Snackbar,
    Switch,
    TextField,
    Typography
} from "@material-ui/core";
import { makeStyles } from "@material-ui/styles";
import ConfigContext from "../contexts/ConfigContext";
import axios from "axios";
import BACKEND_ADDRESS from "../../config";
import { SHA3 } from "sha3";
import Web3 from "web3";
import BaseChangeAAA_abi from "../../contracts/BaseChangeAAA_sol_BaseChangeAAA_abi.json";
import useDynamicRefs from "use-dynamic-refs";
import { Alert } from "@material-ui/lab";
import TransactionResultDialog from "../TransactionResultDialog";
import MetaMaskOnboardingButtonHelper from "../MetaMaskOnboardingButtonHelper";

const useStyles = makeStyles((theme) => ({
    root: {
        minWidth: "100%",
    }
}));


const CustomTextField = ({ initialValue, ...props }) => {
    const [thisText, setThisText] = React.useState(initialValue);

    const updateValue = (event) => {
        setThisText(event.target.value);
    }

    return (<TextField value={thisText} onChange={updateValue} {...props} />);
};


const CustomSwitch = ({ initialValue, ...props }) => {
    const [thisData, setThisData] = React.useState(initialValue);

    const updateValue = (event) => {
        setThisData(event.target.checked);
    };

    return (<Switch checked={thisData} onChange={updateValue} {...props} />)
};


const ChangeConfigContent = () => {
    const classes = useStyles();

    const { websiteHeaderColor } = useContext(ConfigContext);

    const [w3, setW3] = React.useState(null);

    const [changeSchema, setChangeSchema] = React.useState({});
    const [formData, setFormData] = React.useState({});
    const [fieldsOrder, setFieldsOrder] = React.useState("");
    const [changesOrder, setChangesOrder] = React.useState("");

    const [errorSnackbarOpen, setErrorSnackbarOpen] = React.useState(false);
    const [dialogOpen, setDialogOpen] = React.useState(false);
    const [dialogTitle, setDialogTitle] = React.useState("");
    const [dialogContent, setDialogContent] = React.useState("");

    const [getRef, setRef] = useDynamicRefs();

    React.useEffect(() => {
        axios
            .post(BACKEND_ADDRESS.concat("/config/change_protocol_entrypoint"),
                { proto_action: "get_schema" })
            .then(function (response) {
                const { data } = response;
                console.log("SCHEMA RESPONSE: ", response);
                localStorage.setItem("schema", JSON.stringify(data))
                const currentConfig = JSON.parse(localStorage.getItem("currentConfig"));
                setFormData(currentConfig);
                setChangeSchema(data.schema);
                setFieldsOrder(data.fields_order);
                setChangesOrder(data.changes_order);
            })
            .catch(function (error) {
                console.log(error);
            });
    }, []);

    const handleConfigChange = (event) => {
        event.preventDefault();

        const changesToSend = {}
        Object.keys(changeSchema).map((item, index) => {
            if (item !== "nonce") {
                const ref = getRef(item);
                if (item === "boolean_data") {
                    changesToSend[item] = ref.current.checked;
                } else {
                    if (item === "creator") {
                        if (!ref.current.value) {
                            setErrorSnackbarOpen(true);
                            return;
                        }
                    }
                    changesToSend[item] = ref.current.value;
                }
            }
        });
        axios
            .post(BACKEND_ADDRESS.concat("/config/change_protocol_entrypoint"), {
                proto_action: "change_config", payload: { commit: changesToSend },
            })
            .then(function (response) {
                if (response.data.success === 1) {
                    const { commit } = response.data;
                    const { contract_details } = response.data;
                    const hash = new SHA3(512);

                    const commitStructureToHash = {};
                    fieldsOrder.split(",").map((field) => {
                        if (field === "changes") {
                            const changes = {};
                            changesOrder.split(",").map((change) => {
                                changes[change] = change === "boolean_data" ? getRef(change).current.checked : getRef(change).current.value
                            });
                            commitStructureToHash["changes"] = changes;
                        } else if (field === "nonce") {
                            commitStructureToHash["nonce"] = commit.nonce;
                        } else {
                            const value = getRef(field).current.value;

                            if (changeSchema[field] === "[string]") {
                                const list = [];
                                if (value !== "") {
                                    value.split(",").forEach(item => {
                                        list.push(item.trim())
                                    });
                                }
                                commitStructureToHash[field] = list;
                            } else {
                                commitStructureToHash[field] = value;
                            }
                        }
                    });

                    hash.update(JSON.stringify(commitStructureToHash));
                    const hash_value = hash.digest("hex");
                    const hash_beginning = hash_value.substr(0, 12);

                    if (hash_beginning === commit.hash_begins_with) {
                        localStorage.setItem(hash_beginning, JSON.stringify(commitStructureToHash));
                        const baseContract = new w3.eth.Contract(BaseChangeAAA_abi, contract_details.addr);
                        baseContract.methods
                            .createCommit(commitStructureToHash.creator, hash_value, commitStructureToHash.additional_authorizers)
                            .send({ from: commitStructureToHash.creator })
                            .then(function (receipt) {
                                const events = receipt.events;
                                if (events.hasOwnProperty("ChangeCanBeStaged")) {
                                    setDialogTitle("Success");
                                    setDialogContent("Change is about to be staged");
                                    setDialogOpen(true);
                                } else if (events.hasOwnProperty("AuthorizerContractCreated")) {
                                    console.log(events.AuthorizerContractCreated);
                                    const authorizerContract = events.AuthorizerContractCreated.returnValues._authorizerContractAddress;
                                    const hash = events.AuthorizerContractCreated.returnValues._commitHash;
                                    const authorizers = events.AuthorizerContractCreated.returnValues._authorizersNeeded;
                                    const dialogText = `Please tell ${authorizers} to authorize ${hash} on contract with address ${authorizerContract}`;
                                    setDialogContent(dialogText);
                                    setDialogTitle("Authorization is required");
                                    setDialogOpen(true);
                                }
                            });
                    }
                }
            })
            .catch(function (error) {
                console.log(error);
            });
    };

    const handleSnackbarClose = (event, reason) => {
        if (reason === "clickaway") {
            return;
        }

        setErrorSnackbarOpen(false);
    };

    const getFormComponent = (configValue) => {
        const type = changeSchema[configValue];
        let value = formData[configValue];
        if (typeof value === "undefined") {
            value = "";
        }

        if (type === "string" || type === "[string]") {
            return (<CustomTextField initialValue={value} inputRef={setRef(configValue)} key={configValue}
                name={configValue} style={{ marginTop: "15px" }} id={configValue}
                label={configValue} variant="outlined" fullWidth />)
        } else if (type === "bool") {
            return (<FormControlLabel key={configValue} style={{ marginTop: "15px" }}
                control={<CustomSwitch initialValue={value} inputRef={setRef(configValue)}
                    name={configValue} />}
                label={configValue} />);
        } else if (type === "number") {
            return (<CustomTextField initialValue={value} inputRef={setRef(configValue)} style={{ marginTop: "15px" }}
                type="number" label={configValue} key={configValue} variant="outlined" fullWidth />)
        } else if (type === "-") {
            return null;
        }
    };

    return (
        <>
            <Card className={classes.root}>
                <CardContent>
                    <form onSubmit={handleConfigChange} autoComplete="off" noValidate>
                        <Typography variant="h4">Change the configuration</Typography>
                        <Divider />
                        {formData && changeSchema &&
                            Object.keys(changeSchema).map((item, index) => getFormComponent(item))
                        }
                        <Button style={{ marginTop: "15px", backgroundColor: websiteHeaderColor }} type="submit"
                            variant="contained">Submit</Button>
                    </form>
                </CardContent>
            </Card>
            <MetaMaskOnboardingButtonHelper setW3={setW3} />
            <Snackbar open={errorSnackbarOpen} autoHideDuration={5000} onClose={handleSnackbarClose}>
                <Alert onClose={handleSnackbarClose} severity="error">Creator public key must be provided!</Alert>
            </Snackbar>
            <TransactionResultDialog shouldOpen={dialogOpen} setShouldOpen={setDialogOpen} dialogTitle={dialogTitle}
                dialogContent={dialogContent} />
        </>
    );
};

export default ChangeConfigContent;
