import React from "react";
import {
    Button,
    Dialog,
    DialogActions,
    DialogContent,
    DialogContentText,
    DialogTitle, Typography,
} from "@material-ui/core";


const TransactionResultDialog = (props) => {
    const {shouldOpen, setShouldOpen, dialogTitle, dialogContent} = props;

    const handleClose = () => {
        setShouldOpen(false);
    }

    return (
        <Dialog open={shouldOpen} onClose={handleClose} maxWidth="lg" fullWidth={true}>
            <DialogTitle>{dialogTitle}</DialogTitle>
            <DialogContent>
                <DialogContentText>
                    {dialogContent}
                </DialogContentText>
                <DialogActions>
                    <Button onClick={handleClose}>
                        OK
                    </Button>
                </DialogActions>
            </DialogContent>
        </Dialog>
    );
};

export default TransactionResultDialog;