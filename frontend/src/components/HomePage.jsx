import React from "react";
import {Grid, Typography} from "@material-ui/core";
import Header from "./Header";


const HomePage = () => {
    return (
        <>
            <Grid container direction="column">
                <Grid item>
                    <Header pageTitle="Home Page" />
                </Grid>
                <Grid item container>
                    <Grid item xs={false} sm={3}/>
                    <Grid style={{marginTop: "80px"}} item xs={12} sm={6}>
                        <Typography variant="h3">
                            PoC application - DO NOT USE IN PRODUCTION. FOR REFERENCE ONLY.
                        </Typography>
                    </Grid>
                    <Grid item xs={false} sm={3}/>
                </Grid>
            </Grid>
        </>
    )
}

export default HomePage;