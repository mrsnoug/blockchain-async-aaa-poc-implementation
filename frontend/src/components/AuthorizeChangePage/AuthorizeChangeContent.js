import React, {useContext} from "react";
import {makeStyles} from "@material-ui/styles";
import {Button, Card, CardContent, Divider, Snackbar, TextField, Typography} from "@material-ui/core";
import MetaMaskOnboardingButtonHelper from "../MetaMaskOnboardingButtonHelper";
import Authorizer_abi from "../../contracts/Authorizer_sol_Authorizer_abi.json";
import ConfigContext from "../contexts/ConfigContext";
import {Alert} from "@material-ui/lab";

const useStyles = makeStyles((theme) => ({
    root: {
        minWidth: "100%",
    }
}));

const AuthorizeChangeContent = () => {
    const classes = useStyles();
    const {websiteHeaderColor} = useContext(ConfigContext);

    const [w3, setW3] = React.useState(null);
    const [authorizerContractAddress, setAuthorizerContractAddress] = React.useState("");
    const [hashToAuthorize, setHashToAuthorize] = React.useState("");
    const [publicKey, setPublicKey] = React.useState("");

    const [snackbarOpen, setSnackbarOpen] = React.useState(false);
    const [snackbarSeverity, setSnackbarSeverity] = React.useState("");
    const [snackbarContent, setSnackbarContent] = React.useState("");

    const addressOnChange = (event) => {
        setAuthorizerContractAddress(event.target.value);
    };

    const hashOnChange = (event) => {
        setHashToAuthorize(event.target.value);
    };

    const publicKeyOnChange = (event) => {
        setPublicKey(event.target.value);
    };

    const handleOnSubmit = (event) => {
        event.preventDefault();

        const authContract = new w3.eth.Contract(Authorizer_abi, authorizerContractAddress);
        authContract.methods
            .authorize(hashToAuthorize)
            .send({from: publicKey})
            .then(function (receipt) {
                const events = receipt.events;
                if (events.hasOwnProperty("SomeoneAuthorized")) {
                    setSnackbarContent("Authorization was successful");
                    setSnackbarSeverity("success");
                    setSnackbarOpen(true);
                }
            });
    };

    const handleSnackbarClose = (event, reason) => {
        if (reason === "clickaway") {
            return;
        }

        setSnackbarOpen(false);
    }

    return (
        <>
            <Card className={classes.root}>
                <CardContent>
                    <form onSubmit={handleOnSubmit} autoComplete="off" noValidate>
                        <Typography variant="h4">Authorize Change</Typography>
                        <Divider/>
                        <TextField style={{marginTop: "15px"}} value={hashToAuthorize} onChange={hashOnChange} label="Hash of commit to authorize" variant="outlined" fullWidth/>
                        <TextField style={{marginTop: "15px"}} value={authorizerContractAddress} onChange={addressOnChange} label="Authorizer contract address" variant="outlined" fullWidth/>
                        <TextField style={{marginTop: "15px"}} value={publicKey} onChange={publicKeyOnChange} label="Public key of the authorizer" variant="outlined" fullWidth/>
                        <Button style={{marginTop: "15px", backgroundColor: websiteHeaderColor}} type="submit" variant="contained">Authorize</Button>
                    </form>
                </CardContent>
            </Card>
            <MetaMaskOnboardingButtonHelper setW3={setW3}/>
            <Snackbar open={snackbarOpen} autoHideDuration={5000} onClose={handleSnackbarClose}>
                <Alert onClose={handleSnackbarClose} severity={snackbarSeverity}>
                    {snackbarContent}
                </Alert>
            </Snackbar>
        </>
    );
};

export default AuthorizeChangeContent;