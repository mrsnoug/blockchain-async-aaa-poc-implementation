import React, {useContext} from "react";
import {AppBar, Button, Toolbar, Typography} from "@material-ui/core";
import {makeStyles} from "@material-ui/styles";
import {Link} from "react-router-dom";
import ConfigContext from "./contexts/ConfigContext";


const useStyles = makeStyles((theme) => ({
    appbar: {
        marginBottom: "10px",
        color: "#FFFFFFA0",
        //zIndex: theme.zIndex.drawer + 1,
    },
    button: {
        color: "inherit",
    },
    title: {
        flexGrow: 1,
    }
}));

const Header = (props) => {
    const classes = useStyles();
    const {pageTitle} = props;
    const {websiteHeaderColor} = useContext(ConfigContext);

    return (
        <AppBar className={classes.appbar} style={{backgroundColor: websiteHeaderColor}} position="fixed">
            <Toolbar>
                <Typography variant="h6" className={classes.title}>
                    {pageTitle}
                </Typography>
                <Button className={classes.button}>
                    <Link style={{color: "#ffffffa0", textDecoration: "inherit"}} to="/check_config">Check Config</Link>
                </Button>
                <Button className={classes.button}>
                    <Link style={{color: "#ffffffa0", textDecoration: "inherit"}} to="/check_changes">Check Changes</Link>
                </Button>
                <Button className={classes.button}>
                    <Link style={{color: "#ffffffa0", textDecoration: "inherit"}} to="/change_config">Change Config</Link>
                </Button>
                <Button className={classes.button}>
                    <Link style={{color: "#ffffffa0", textDecoration: "inherit"}} to="/authorize_change">Authorize Change</Link>
                </Button>
            </Toolbar>
        </AppBar>
    )
}

export default Header;