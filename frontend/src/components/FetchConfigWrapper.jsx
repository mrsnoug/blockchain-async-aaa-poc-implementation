import React, { useContext, useEffect } from "react";
import { Route, useLocation } from "react-router-dom";
import ConfigContext from "./contexts/ConfigContext";
import axios from "axios";
import BACKEND_ADDRESS from "../config";


const FetchConfigWrapper = ({ children, ...rest }) => {
    const { pathname } = useLocation();
    const {
        setNumberData,
        setStringData,
        setBooleanData,
        setWebsiteHeaderColor,
    } = useContext(ConfigContext);

    useEffect(() => {
        axios
            .post(BACKEND_ADDRESS.concat("/config/change_protocol_entrypoint"),
                { proto_action: "get_config" },
            )
            .then(function (response) {
                const { data } = response;
                console.log(response);
                setStringData(data.string_data);
                setBooleanData(data.boolean_data.toString());
                setNumberData(data.number_data.toString());
                setWebsiteHeaderColor(data.website_header_color);
                localStorage.setItem("currentConfig", JSON.stringify(data));
            })
            .catch(function (error) {
                console.log(error);
            });
    }, [pathname]);

    return (
        <Route {...rest}>
            {children}
        </Route>
    );
}

export default FetchConfigWrapper;