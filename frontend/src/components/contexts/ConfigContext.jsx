import React from "react";


const ConfigContext = React.createContext({
    stringData: "", setStingData: () => {
    }, booleanData: null, setBooleanData: () => {
    }, numberData: null, setNumberData: () => {
    }, websiteHeaderColor: null, setWebsiteHeaderColor: () => {
    },
});

export default ConfigContext;
