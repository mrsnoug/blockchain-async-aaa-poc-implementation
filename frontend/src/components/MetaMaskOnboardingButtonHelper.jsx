import React from "react";
import MetaMaskOnboarding from "@metamask/onboarding";
import {Button} from "@material-ui/core";
import Web3 from "web3";

const ONBOARD_TEXT = "Click here to install MetaMask";
const CONNECT_TEXT = "Connect";
const CONNECTED_TEXT = "Connected to MetaMask";

const MetaMaskOnboardingButtonHelper = (props) => {
    const {setW3} = props;

    const onboarding = React.useRef(null);
    const [buttonDisabled, setButtonDisabled] = React.useState(false);
    const [buttonText, setButtonText] = React.useState(ONBOARD_TEXT);
    const [ethAccounts, setEthAccounts] = React.useState([]);

    React.useEffect(() => {
        if (!onboarding.current) {
            onboarding.current = new MetaMaskOnboarding();
            if (MetaMaskOnboarding.isMetaMaskInstalled()) {
                window.ethereum
                    .request({method: "eth_requestAccounts"})
                    .then((accounts) => setEthAccounts(accounts));
                window.ethereum.on("accountsChanged", (accounts) => setEthAccounts(accounts));
            }
        }
    }, []);

    React.useEffect(() => {
        if (MetaMaskOnboarding.isMetaMaskInstalled()) {
            if (ethAccounts.length > 0) {
                setButtonText(CONNECTED_TEXT);
                setButtonDisabled(true);
                onboarding.current.stopOnboarding();
                const web3 = new Web3(Web3.givenProvider);
                setW3(web3);
            } else {
                setButtonText(CONNECT_TEXT);
                setButtonDisabled(false);
            }
        }
    }, [ethAccounts]);

    const connectToMetaMaskOnClick = () => {
        if (MetaMaskOnboarding.isMetaMaskInstalled()) {
            window.ethereum
                .request({method: "eth_requestAccounts"})
                .then((accounts) => setEthAccounts(accounts));
        } else {
            onboarding.current.startOnboarding();
        }
    };

    return (
        <Button style={{marginTop: "10px"}} variant="outlined" disabled={buttonDisabled}
                onClick={connectToMetaMaskOnClick}>{buttonText}</Button>
    );
};

export default MetaMaskOnboardingButtonHelper;
