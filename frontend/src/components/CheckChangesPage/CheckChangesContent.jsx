import React from "react";
import { Button, Card, CardContent, Divider, Snackbar, TextField, Typography } from "@material-ui/core";
import { makeStyles } from "@material-ui/styles";
import ConfigContext from "../contexts/ConfigContext";
import axios from "axios";
import BACKEND_ADDRESS from "../../config";
import { Alert } from "@material-ui/lab";
import TransactionResultDialog from "../TransactionResultDialog";
import BaseChangeAAA_abi from "../../contracts/BaseChangeAAA_sol_BaseChangeAAA_abi.json";
import MetaMaskOnboardingButtonHelper from "../MetaMaskOnboardingButtonHelper";

const useStyles = makeStyles((theme) => ({
    root: {
        minWidth: "100%",
    }
}));

const CheckChangesContent = () => {
    const classes = useStyles();
    const { websiteHeaderColor } = React.useContext(ConfigContext);

    const [commitHash, setCommitHash] = React.useState("");
    const [requesterPublicKey, setRequesterPublicKey] = React.useState("");

    const [snackbarOpen, setSnackbarOpen] = React.useState(false);
    const [snackbarSeverity, setSnackbarSeverity] = React.useState("");
    const [snackbarText, setSnackbarText] = React.useState("");
    const [dialogOpen, setDialogOpen] = React.useState(false);
    const [dialogTitle, setDialogTitle] = React.useState("");
    const [dialogContent, setDialogContent] = React.useState("");

    const [w3, setW3] = React.useState(null);

    const commitHashOnChange = (event) => {
        setCommitHash(event.target.value);
    };

    const requesterPublicKeyOnChange = (event) => {
        setRequesterPublicKey(event.target.value);
    };

    const handleOnSubmit = (event) => {
        event.preventDefault();
        const token = localStorage.getItem("token");
        const postData = {}
        postData["commit_hash"] = commitHash;
        postData["public_key"] = requesterPublicKey;
        if (token) {
            postData["token"] = token;
        }

        axios
            .post(BACKEND_ADDRESS.concat("/config/change_protocol_entrypoint"), {
                proto_action: "get_change_from_hash", payload: postData,
            })
            .then(function (response) {
                console.log(response);
                const { data } = response;
                console.log("CHECK CHANGES DATA: ", data);
                if (data.success === 1) {
                    if (data.hasOwnProperty("token_value") && data.hasOwnProperty("challenge") && data.hasOwnProperty("contract_address")) {
                        localStorage.setItem("token", data.token_value);
                        setSnackbarSeverity("success");
                        setSnackbarText("Please authenticate token via blockchain");
                        setSnackbarOpen(true);
                        const baseContract = new w3.eth.Contract(BaseChangeAAA_abi, data.contract_address);
                        baseContract.methods
                            .authenticateToken(data.challenge)
                            .send({ from: requesterPublicKey })
                            .then(function (receipt) {
                                const events = receipt.events;
                                console.log(events);
                                if (events.hasOwnProperty("TokenAuthenticated")) {
                                    setSnackbarText("Token was authenticated. You should be able to get the changes soon. Try to request the changes again.");
                                    setSnackbarSeverity("success");
                                    setSnackbarOpen(true);
                                }
                            });
                    } else if (data.hasOwnProperty("changes")) {
                        const changes = data.changes;
                        localStorage.removeItem("token");
                        setDialogTitle("Changes for this commit");
                        const content_text = `Boolean data: ${changes.boolean_data}, Number data: ${changes.number_data}, String data: ${changes.string_data}, Website header color: ${changes.website_header_color}`;
                        setDialogContent(content_text);
                        setDialogOpen(true);
                    } else {
                        setSnackbarSeverity("error");
                        setSnackbarText("UNDEFINED RESPONSE");
                        setSnackbarOpen(true);
                    }
                } else {
                    console.log(data.message);
                    setSnackbarSeverity("error");
                    setSnackbarText(data.message);
                    setSnackbarOpen(true);
                    localStorage.removeItem("token");
                }
            })
            .catch(function (error) {
                console.log(error);
            })
        console.log(commitHash);
    };

    const handleSnackbarClose = (event, reason) => {
        if (reason === "clickaway") {
            return;
        }

        setSnackbarOpen(false);
    }

    return (
        <>
            <Card className={classes.root}>
                <CardContent>
                    <form onSubmit={handleOnSubmit} autoComplete="off" noValidate>
                        <Typography variant="h4">Check changes</Typography>
                        <Divider />
                        <TextField value={commitHash} onChange={commitHashOnChange} label="Commit hash"
                            style={{ marginTop: "15px" }} variant="outlined" fullWidth />
                        <TextField value={requesterPublicKey} onChange={requesterPublicKeyOnChange}
                            label="Requester's public key" style={{ marginTop: "15px" }} variant="outlined"
                            fullWidth />
                        <Button style={{ marginTop: "15px", backgroundColor: websiteHeaderColor }} type="submit"
                            variant="contained">Submit</Button>
                    </form>
                </CardContent>
            </Card>
            <MetaMaskOnboardingButtonHelper setW3={setW3} />
            <TransactionResultDialog shouldOpen={dialogOpen} setShouldOpen={setDialogOpen} dialogTitle={dialogTitle}
                dialogContent={dialogContent} />
            <Snackbar open={snackbarOpen} autoHideDuration={5000} onClose={handleSnackbarClose}>
                <Alert onClose={handleSnackbarClose} severity={snackbarSeverity}>
                    {snackbarText}
                </Alert>
            </Snackbar>
        </>
    );
};

export default CheckChangesContent;
