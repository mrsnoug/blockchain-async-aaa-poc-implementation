import React from "react";
import {Grid} from "@material-ui/core";
import Header from "../Header";
import CheckChangesContent from "./CheckChangesContent";

const CheckChangesPage = () => {
        return (
        <Grid container direction="column">
            <Grid item>
                <Header pageTitle="Check Change"/>
            </Grid>
            <Grid item container style={{marginTop: "80px"}}>
                <Grid item xs={false} sm={3}/>
                <Grid item container xs={12} sm={6}>
                    <CheckChangesContent/>
                </Grid>
                <Grid item xs={false} sm={3}/>
            </Grid>
        </Grid>
    );
};

export default CheckChangesPage;
