rm -f ./frontend/src/contracts/*

cd ./BlockchainAAA/contracts/. || exit

solcjs --abi --bin BaseChangeAAA.sol Authorizer.sol

cp ./Authorizer_sol_Authorizer.abi ../../frontend/src/contracts/Authorizer_sol_Authorizer_abi.json
cp ./BaseChangeAAA_sol_BaseChangeAAA.abi ../../frontend/src/contracts/BaseChangeAAA_sol_BaseChangeAAA_abi.json

echo "DONE"
