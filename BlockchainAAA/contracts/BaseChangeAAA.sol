// SPDX-License-Identifier: UNLICENSED
pragma solidity =0.8.13;

import "./Authorizer.sol";

// THIS CONTRACT IS NOT AUDITED AND SHOULD NOT BE CONSIDERED AS A BEST PRACTICE EXAMPLE.
// THIS CONTRACT SHOULD BE CONSIDERED ONLY AS A REFERENCE FOR THE RAW FUNCTIONALITY.
// It does not implement any additional funcionalities, which would be expected from the
// smart contract, for example: a pausable feature. What's more, it deliberately does not derive
// from the well-known contract base (for example OpenZeppelin one) not to cloud any functionality.
contract BaseChangeAAA {
    struct Commit {
        bool _needsAuthorization;
        uint256 _authorizersLeft;
        mapping(address => bool) _authorizers;
        address _commitCreator;
        bool _exists; // to check if commit actually exists, because mapping will always return something
    }

    mapping(string => Commit) _hashToCommit;
    address _server;
    mapping(address => bool) private _isAddressAdmin;
    mapping(address => bool) private _isItADeployedAuthorizerContracts;

    event BaseContractCreated(address _baseAddress);
    event AuthorizerContractCreated(
        address indexed _authorizerContractAddress,
        string _commitHash,
        address[] _authorizersNeeded
    );
    event ChangeCanBeStaged(string _commitHash, address indexed _creator);
    event TokenAuthenticated(address indexed _address, string _challenge);

    modifier onlyServer() {
        require(msg.sender == _server, "sender is not a server");
        _;
    }

    modifier onlyAuthorizer() {
        require(
            _isItADeployedAuthorizerContracts[msg.sender],
            "Sender needs to be one of the deployed Authorizer Contracts"
        );
        _;
    }

    constructor(address[] memory admins) {
        _server = msg.sender;
        for (uint256 i = 0; i < admins.length; i++) {
            _isAddressAdmin[admins[i]] = true;
        }
        emit BaseContractCreated(address(this));
    }

    function authenticateToken(string memory _challenge) public {
        emit TokenAuthenticated(msg.sender, _challenge);
    }

    function createCommit(
        address _commitCreator,
        string memory _commitHash,
        address[] memory _commitAuthorizers
    ) public {
        require(_commitCreator == msg.sender, "Invalid sender");
        bool _authorizationNeeded = true;

        Commit storage newCommit = _hashToCommit[_commitHash];

        if (newCommit._exists) {
            revert("Commit with that hash already exists!");
        }

        if (_isAddressAdmin[_commitCreator]) {
            _authorizationNeeded = false;
        }

        if (_authorizationNeeded) {
            bool _allAuthorizersValid = true;

            require(
                _commitAuthorizers.length > 0,
                "Authorization is needed, but no additional autorizers were provided"
            );

            for (uint256 i = 0; i < _commitAuthorizers.length; i++) {
                if (!_isAddressAdmin[_commitAuthorizers[i]]) {
                    _allAuthorizersValid = false;
                    break;
                } else {
                    newCommit._authorizers[_commitAuthorizers[i]] = true;
                }
            }

            require(
                _allAuthorizersValid,
                "At least one authorizer was provided who has no authorization privileges"
            );

            newCommit._needsAuthorization = true;
            newCommit._authorizersLeft = _commitAuthorizers.length;
            newCommit._commitCreator = _commitCreator;
            newCommit._exists = true;

            createAuthorizerContract(_commitHash, _commitAuthorizers);
        } else {
            emit ChangeCanBeStaged(_commitHash, _commitCreator);
        }
    }

    function createAuthorizerContract(
        string memory _hash,
        address[] memory _commitAuthorizers
    ) private {
        Authorizer auth = new Authorizer(
            address(this),
            _hash,
            _commitAuthorizers
        );
        _isItADeployedAuthorizerContracts[address(auth)] = true;
        emit AuthorizerContractCreated(
            address(auth),
            _hash,
            _commitAuthorizers
        );
    }

    function someoneAuthorizedChange(string memory _hash, address _whoDidIt)
        external
        onlyAuthorizer
    {
        Commit storage _relatedCommit = _hashToCommit[_hash];

        require(_relatedCommit._exists, "This commit does not exist");
        require(
            _relatedCommit._needsAuthorization,
            "There is no commit with that hash value which requires authorization"
        );

        if (_relatedCommit._authorizers[_whoDidIt]) {
            _relatedCommit._authorizers[_whoDidIt] = false;
            _relatedCommit._authorizersLeft -= 1;
        }

        if (_relatedCommit._authorizersLeft == 0) {
            emit ChangeCanBeStaged(_hash, _relatedCommit._commitCreator);
            delete _hashToCommit[_hash];
            Authorizer _auth = Authorizer(msg.sender);
            _auth.goOffline();
        }
    }
}
