// SPDX-License-Identifier: UNLICENSED
pragma solidity =0.8.13;

import "./BaseChangeAAA.sol";

// THIS CONTRACT IS NOT AUDITED AND SHOULD NOT BE CONSIDERED AS A BEST PRACTICE EXAMPLE.
// THIS CONTRACT SHOULD BE CONSIDERED ONLY AS A REFERENCE FOR THE RAW FUNCTIONALITY.
// It does not implement any additional funcionalities, which would be expected from the
// smart contract, for example: a pausable feature. What's more, it deliberately does not derive
// from the well-known contract base (for example OpenZeppelin one) not to cloud any functionality.
contract Authorizer {
    BaseChangeAAA _base;
    mapping(address => bool) _authorizerNeeded;
    string _commitHash;
    bool _isOnline;

    event SomeoneAuthorized(string indexed what, address indexed _who);

    modifier onlyBase() {
        require(
            msg.sender == address(_base),
            "Only BaseChangeAAA contract can access this"
        );
        _;
    }

    modifier isOnline() {
        require(_isOnline == true, "Contract is no longer online");
        _;
    }

    modifier validHash(string memory hash) {
        require(
            keccak256(abi.encodePacked(hash)) ==
                keccak256(abi.encodePacked(_commitHash)),
            "This is not the expected hash value"
        );
        _;
    }

    constructor(
        address _baseAddress,
        string memory _hash,
        address[] memory _authorizers
    ) {
        _base = BaseChangeAAA(_baseAddress);
        _commitHash = _hash;

        for (uint256 i = 0; i < _authorizers.length; i++) {
            _authorizerNeeded[_authorizers[i]] = true;
        }

        _isOnline = true;
    }

    function goOffline() external onlyBase {
        _isOnline = false;
    }

    function authorize(string memory hash) public isOnline validHash(hash) {
        if (_authorizerNeeded[msg.sender]) {
            _base.someoneAuthorizedChange(_commitHash, msg.sender);
            emit SomeoneAuthorized(_commitHash, msg.sender);
        }
    }
}
