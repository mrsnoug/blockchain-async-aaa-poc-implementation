from django.db import models
from django.contrib.auth.models import User
import hashlib
import json
import secrets


class CommitManager(models.Manager):
    def safe_get(self, *args, **kwargs):
        try:
            obj = self.get(*args, **kwargs)
        except Commit.DoesNotExist:
            obj = None

        return obj


class Commit(models.Model):
    changes = models.JSONField()
    creator = models.ForeignKey("BlockchainUser", null=True, on_delete=models.SET_NULL)
    additional_authorizers = models.ManyToManyField(
        "BlockchainUser", related_name="authorized_by", blank=True
    )
    depends_on = models.ManyToManyField("self", blank=True)
    commit_hash = models.CharField(
        max_length=128, null=False, default="N/A"
    )  # hashes are 512 bits long, so 128 nibbles
    nonce = models.PositiveBigIntegerField()
    is_staged = models.BooleanField(default=False)
    is_commit_authorized = models.BooleanField(default=False)

    objects = CommitManager()

    def save(self, *args, **kwargs):
        if self.creator and self.changes:
            commit = {
                "changes": self.changes,
                "creator": self.creator.user.username,
            }

            if not self.nonce:
                self.nonce = secrets.randbits(32)

            commit["nonce"] = self.nonce

            if self.pk:
                if self.additional_authorizers:
                    commit["additional_authorizers"] = [
                        authorizer.user.username
                        for authorizer in self.additional_authorizers.all()
                    ]
                else:
                    commit["additional_authorizers"] = None

                if self.depends_on:
                    commit["depends_on"] = [
                        com.commit_hash for com in self.depends_on.all()
                    ]
                else:
                    commit["depends_on"] = None

            self.commit_hash = hashlib.sha3_512(
                bytes(json.dumps(commit, separators=(",", ":")), "utf-8")
            ).hexdigest()
            super(Commit, self).save(*args, **kwargs)


class SingletonModel(models.Model):
    class Meta:
        abstract = True

    def save(self, *args, **kwargs):
        self.pk = 1
        super(SingletonModel, self).save(*args, **kwargs)

    def delete(self, *args, **kwargs):
        pass

    @classmethod
    def load(cls):
        obj, created = cls.objects.get_or_create(pk=1)
        return obj


class Configuration(SingletonModel):
    string_data = models.CharField(max_length=512, default="abcdef")
    boolean_data = models.BooleanField(default=False)
    number_data = models.FloatField(default=0.0)
    website_header_color = models.CharField(max_length=32, default="blue")


class ConfigurationSchema(SingletonModel):
    schema = models.JSONField()
    schema_hash = models.CharField(max_length=64, default="N/A")  # sha3_256
    fields_order = models.CharField(max_length=64, default="N/A")
    changes_order = models.CharField(max_length=64, default="N/A")

    @staticmethod
    def get_datatype(field):
        if isinstance(field, models.CharField):
            return "string"
        elif isinstance(field, models.BooleanField):
            return "bool"
        elif isinstance(field, models.FloatField):
            return "number"
        elif isinstance(field, models.ManyToManyField):
            return "[string]"
        elif isinstance(field, models.ForeignKey):
            return "string"
        else:
            return "-"

    def save(self, *args, **kwargs):
        self.fields_order = "changes,creator,nonce,additional_authorizers,depends_on"
        self.changes_order = "boolean_data,number_data,string_data,website_header_color"
        current_schema = {}
        for config_field in Configuration._meta.get_fields():
            if config_field.name == "id":
                continue

            current_schema[config_field.name] = self.get_datatype(config_field)

        for commit_field in Commit._meta.get_fields():
            if (
                commit_field.name == "id"
                or commit_field.name == "is_staged"
                or commit_field.name == "is_commit_authorized"
                or commit_field.name == "changes"
                or commit_field.name == "commit_hash"
                or commit_field.name == "token"
            ):
                continue

            current_schema[commit_field.name] = self.get_datatype(commit_field)

        self.schema = current_schema
        self.schema_hash = hashlib.sha3_256(
            bytes(json.dumps(current_schema, separators=(",", ":")), "utf-8")
        ).hexdigest()
        super(ConfigurationSchema, self).save(*args, **kwargs)


class ServerBlockchainConfiguration(SingletonModel):
    server_address = models.CharField(max_length=512, default="N/A")
    contract_address = models.CharField(max_length=512, default="N/A")
    contract_abi = models.CharField(max_length=4096, default="N/A")
    authorizer_abi = models.CharField(max_length=4096, default="N/A")


class BlockchainUserManager(models.Manager):
    def safe_get(self, *args, **kwargs):
        try:
            obj = self.get(*args, **kwargs)
        except BlockchainUser.DoesNotExist:
            obj = None

        return obj


class BlockchainUser(models.Model):
    user = models.OneToOneField(User, models.CASCADE, primary_key=True)
    human_friendly_name = models.CharField(max_length=256, null=True)

    # private_key is not used anywhere and is here just to make it easier to import accounts to MetaMask
    # It will be a valid private_key if ganache-cli was run with -d option
    private_key = models.CharField(max_length=72, default="N/A")

    objects = BlockchainUserManager()

    def __str__(self):
        return f"{self.human_friendly_name} | {self.user.username}"


class TokenManager(models.Manager):
    def safe_get(self, *args, **kwargs):
        try:
            obj = self.get(*args, **kwargs)
        except Commit.DoesNotExist:
            obj = None

        return obj


class Token(models.Model):
    bc_user = models.ForeignKey(BlockchainUser, on_delete=models.SET_NULL, null=True)
    commit = models.ForeignKey(Commit, on_delete=models.SET_NULL, null=True)
    token_value = models.CharField(max_length=128)
    challenge = models.CharField(max_length=128, default="N/A")
    is_exhausted = models.BooleanField(default=False)
    is_authenticated = models.BooleanField(default=False)

    objects = TokenManager()

    def generate_token_and_challenge(self):
        self.token_value = secrets.token_hex(64)
        self.challenge = hashlib.sha3_512(bytes(self.token_value, "utf-8")).hexdigest()

    def save(self, *args, **kwargs):
        if self.bc_user and self.commit:
            super(Token, self).save(*args, **kwargs)
