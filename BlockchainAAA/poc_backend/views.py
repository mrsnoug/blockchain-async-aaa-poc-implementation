from django.http import HttpResponse, JsonResponse
from rest_framework import views
from .models import (
    Configuration,
    Commit,
    BlockchainUser,
    ServerBlockchainConfiguration,
    ConfigurationSchema,
    Token,
)
from .serializers import (
    ConfigurationSerializer,
    ConfigurationSchemaSerializer,
)


class ConfigurationView:
    @staticmethod
    def process():
        configuration = Configuration.load()
        serializer = ConfigurationSerializer(configuration)
        return JsonResponse(serializer.data)


class ChangeConfigurationView:
    @staticmethod
    def get_valid_manytomany(model_class, list_to_check):
        if len(list_to_check) == 1:
            if list_to_check[0] == "":
                return True, [], ""

        if model_class is BlockchainUser:
            list_of_model_objects = [
                model_class.objects.safe_get(user__username=name)
                for name in list_to_check
            ]
        elif model_class is Commit:
            list_of_model_objects = [
                model_class.objects.safe_get(commit_hash=comm_hash)
                for comm_hash in list_to_check
            ]
        else:
            raise Exception("WRONG MODEL CLASS")

        list_of_model_objects = [
            obj for obj in list_of_model_objects if obj is not None
        ]
        if len(list_of_model_objects) != len(list_to_check):
            return (
                False,
                list_of_model_objects,
                "At least one of objects does not exist",
            )

        if model_class is BlockchainUser:
            list_of_model_objects = [
                obj
                for obj in list_of_model_objects
                if obj.user.is_staff or obj.user.is_superuser
            ]
            if len(list_of_model_objects) != len(list_to_check):
                return (
                    False,
                    list_of_model_objects,
                    "These blockchain users cannot authorize that change",
                )

        return True, list_of_model_objects, ""

    @staticmethod
    def process(data):
        new_boolean_data = data["boolean_data"]
        new_number_data = data["number_data"]
        new_string_data = data["string_data"]
        new_website_header_color = data["website_header_color"]
        creator_public_key = data["creator"]
        authorized_by_list = [
            authorizer.strip()
            for authorizer in data["additional_authorizers"].split(",")
        ]
        depends_on_list = [commit.strip() for commit in data["depends_on"].split(",")]

        creator = BlockchainUser.objects.safe_get(user__username=creator_public_key)
        if creator is None:
            response_data = {
                "success": 0,
                "message": "This blockchain user does not exist",
            }
            return JsonResponse(data=response_data)

        (
            authorizers_valid,
            list_of_valid_authorizers,
            message,
        ) = ChangeConfigurationView.get_valid_manytomany(
            BlockchainUser, authorized_by_list
        )
        if not authorizers_valid:
            response_data = {"success": 0, "message": message}
            return JsonResponse(data=response_data)

        (
            preceding_commits_valid,
            list_of_valid_preceding_commits,
            message,
        ) = ChangeConfigurationView.get_valid_manytomany(Commit, depends_on_list)
        if not preceding_commits_valid:
            response_data = {"success": 0, "message": message}
            return JsonResponse(data=response_data)

        changes = {
            "boolean_data": new_boolean_data,
            "number_data": new_number_data,
            "string_data": new_string_data,
            "website_header_color": new_website_header_color,
        }

        commit = Commit(changes=changes, creator=creator)
        commit.save()

        for authorizer in list_of_valid_authorizers:
            commit.additional_authorizers.add(authorizer)

        for dependant_commit in list_of_valid_preceding_commits:
            commit.depends_on.add(dependant_commit)

        commit.save()
        blockchain_config = ServerBlockchainConfiguration.load()
        response_data = {
            "success": 1,
            "commit": {
                "hash_begins_with": commit.commit_hash[:12],
                "nonce": commit.nonce,
            },
            "contract_details": {
                "addr": blockchain_config.contract_address,
                "abi": blockchain_config.contract_abi,
            },
        }
        return JsonResponse(data=response_data)


class GetConfigurationSchemaView:
    @staticmethod
    def process():
        ConfigurationSchema.load().save()
        schema = ConfigurationSchema.load()  # because schema is updated on save
        return JsonResponse(data=ConfigurationSchemaSerializer(schema).data)


class GetChangeFromHash:
    @staticmethod
    def process(data):
        print(f"DATA: {data}")
        requested_commit_hash = data.get("commit_hash")
        token = data.get("token")
        if not token:
            public_key = data.get("public_key")
            bc_user = BlockchainUser.objects.safe_get(user__username=public_key)
            if not bc_user:
                return JsonResponse(
                    data={
                        "success": 0,
                        "message": "There is no Blockchain User with this public key",
                    }
                )

            commit = Commit.objects.safe_get(commit_hash=requested_commit_hash)
            if not commit:
                return JsonResponse(
                    data={"success": 0, "message": "There is no commit with that hash"}
                )

            if not (
                (bc_user.user.is_staff and bc_user.user.is_superuser)
                or commit.creator == bc_user
            ):
                return JsonResponse(
                    data={
                        "success": 0,
                        "message": "Token can be created only for admin or commit creator",
                    }
                )

            new_token = Token(bc_user=bc_user, commit=commit)
            new_token.generate_token_and_challenge()
            new_token.save()
            print(f"{new_token.token_value=}, {new_token.challenge}")
            blockchain_config = ServerBlockchainConfiguration.load()
            return JsonResponse(
                data={
                    "success": 1,
                    "token_value": new_token.token_value,
                    "challenge": new_token.challenge,
                    "contract_address": blockchain_config.contract_address,
                    "abi": blockchain_config.contract_abi,
                }
            )
        else:
            print(token)
            token_object = Token.objects.safe_get(token_value=token)
            if not token_object:
                return JsonResponse(
                    data={"success": 0, "message": "Invalid token value"}
                )

            if not token_object.is_authenticated:
                return JsonResponse(
                    data={
                        "success": 0,
                        "message": "This token is not yet authenticated",
                    }
                )

            if token_object.is_exhausted:
                return JsonResponse(
                    data={"success": 0, "message": "This token was already exhausted"}
                )

            commit = Commit.objects.safe_get(commit_hash=requested_commit_hash)
            if token_object.commit != commit:
                return JsonResponse(
                    data={
                        "success": 0,
                        "message": "This token is not associated with requested commit",
                    }
                )

            token_object.is_exhausted = True
            token_object.save()
            return JsonResponse(
                data={
                    "success": 1,
                    "changes": commit.changes,
                }
            )


class GetAbiView:
    @staticmethod
    def process(data):
        abi_type = data.get("abi_type")
        blockchain_config = ServerBlockchainConfiguration.load()

        if abi_type == "base":
            return JsonResponse(
                data={"success": 1, "abi": blockchain_config.contract_abi}
            )
        elif abi_type == "authorizer":
            return JsonResponse(
                data={"success": 1, "abi": blockchain_config.authorizer_abi}
            )

        return JsonResponse(data={"success": 0, "message": "Invalid abi type"})


class ProtocolEntryPoint(views.APIView):
    def post(self, request):
        print(request)
        action = request.data.get("proto_action")
        payload = request.data.get("payload")

        if action == "get_config":
            print("Get config")
            return ConfigurationView.process()
        elif action == "get_schema":
            print("Get schema")
            return GetConfigurationSchemaView.process()
        elif action == "change_config":
            print("Change config")
            if "commit" not in payload:
                return JsonResponse(
                    data={"success": 0, "message": "No changes in payload"}
                )

            return ChangeConfigurationView.process(payload["commit"])
        elif action == "get_change_from_hash":
            print("Get change from hash")
            return GetChangeFromHash.process(payload)
        elif action == "get_abi":
            print("Get abi")
            return GetAbiView.process(payload)

        return JsonResponse(data={"success": 0, "message": "Not a valid request"})
