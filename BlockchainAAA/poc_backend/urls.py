from django.urls import path

from . import views

urlpatterns = [
    # path("get_config", views.ConfigurationView.as_view()),
    # path("change_config", views.ChangeConfigurationView.as_view()),
    # path("get_schema", views.GetConfigurationSchemaView.as_view()),
    # path("get_change_from_hash", views.GetChangeFromHash.as_view()),
    path("change_protocol_entrypoint", views.ProtocolEntryPoint.as_view()),
]
