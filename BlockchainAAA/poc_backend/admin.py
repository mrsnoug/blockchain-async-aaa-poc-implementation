from django.contrib import admin
from .models import *

admin.site.register(Commit)
admin.site.register(BlockchainUser)
admin.site.register(Configuration)
admin.site.register(ServerBlockchainConfiguration)
admin.site.register(ConfigurationSchema)
admin.site.register(Token)
