# Generated by Django 3.2.3 on 2021-07-26 17:02

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('poc_backend', '0002_alter_commit_creator'),
    ]

    operations = [
        migrations.CreateModel(
            name='ServerBlockchainConfiguration',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('server_address', models.CharField(default='N/A', max_length=512)),
                ('contract_address', models.CharField(default='N/A', max_length=512)),
            ],
            options={
                'abstract': False,
            },
        ),
    ]
