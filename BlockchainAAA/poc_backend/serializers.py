from rest_framework import serializers
from .models import Configuration, Commit, BlockchainUser, ConfigurationSchema
from django.contrib.auth.models import User


class ConfigurationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Configuration
        fields = ["string_data", "boolean_data", "number_data", "website_header_color"]


class ConfigurationSchemaSerializer(serializers.ModelSerializer):
    class Meta:
        model = ConfigurationSchema
        fields = ["schema", "schema_hash", "fields_order", "changes_order"]


class DjangoUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ["username"]


class BlockchainUserSerializer(serializers.ModelSerializer):
    user = DjangoUserSerializer()

    class Meta:
        model = BlockchainUser
        fields = "__all__"
        depth = 1


class DependsOnSerializer(serializers.ModelSerializer):
    class Meta:
        model = Commit
        fields = ["commit_hash"]


class CommitSerializer(serializers.ModelSerializer):
    creator = BlockchainUserSerializer()
    additional_authorizers = BlockchainUserSerializer(many=True)
    depends_on = DependsOnSerializer(many=True)

    class Meta:
        model = Commit
        fields = "__all__"
