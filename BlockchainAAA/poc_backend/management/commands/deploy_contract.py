import os
import json
from web3 import HTTPProvider, Web3
from django.core.management import BaseCommand

from ...models import ServerBlockchainConfiguration, BlockchainUser


class Command(BaseCommand):
    help = "Compiles a contract, publishes it on the blockchain and saves the contract address in db"

    def handle(self, *args, **options):
        admin_public_keys = [
            bcu.user.username
            for bcu in BlockchainUser.objects.filter(
                user__is_staff=True, user__is_superuser=True
            )
        ]
        ethereum_node = os.environ.get("ETHEREUM_NODE_URI", "http://127.0.0.1:8545")
        w3 = Web3(HTTPProvider(ethereum_node))
        server = w3.eth.accounts[0]
        abi = open("contracts/BaseChangeAAA_sol_BaseChangeAAA.abi").read()
        bytecode = open("contracts/BaseChangeAAA_sol_BaseChangeAAA.bin").read()
        contract = w3.eth.contract(abi=abi, bytecode=bytecode)
        tx_hash = contract.constructor(admin_public_keys).transact(
            transaction={"from": server}
        )
        tx_receipt = w3.eth.wait_for_transaction_receipt(tx_hash)
        contract_address = tx_receipt["contractAddress"]

        authorizer_abi = open("contracts/Authorizer_sol_Authorizer.abi").read()

        conf = ServerBlockchainConfiguration.load()
        conf.contract_abi = json.dumps(abi)
        conf.authorizer_abi = json.dumps(authorizer_abi)
        conf.contract_address = contract_address
        conf.server_address = server
        conf.save()
