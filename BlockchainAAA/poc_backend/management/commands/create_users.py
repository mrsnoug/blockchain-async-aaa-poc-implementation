from django.core.management import BaseCommand
from web3 import Web3, HTTPProvider
import os

from ...models import BlockchainUser
from django.contrib.auth.models import User


class Command(BaseCommand):
    help = "Creates loggable admin user, two blockchain admin user and one regular blockchain user"

    def handle(self, *args, **options):
        should_create_superuser = False
        try:
            admin = User.objects.get(username="admin")
        except User.DoesNotExist:
            should_create_superuser = True

        if should_create_superuser:
            User.objects.create_superuser("admin", "admin@admin.com", "admin")

        eth_node = os.environ.get("ETHEREUM_NODE_URI", "http://127.0.0.1:8545")
        w3 = Web3(HTTPProvider(eth_node))

        first_admin_username = w3.eth.accounts[1]
        second_admin_username = w3.eth.accounts[2]
        regular_user_username = w3.eth.accounts[3]

        should_create_user1 = False
        should_create_user2 = False
        should_create_user3 = False

        try:
            u1 = User.objects.get(username=first_admin_username)
        except User.DoesNotExist:
            should_create_user1 = True

        try:
            u2 = User.objects.get(username=second_admin_username)
        except User.DoesNotExist:
            should_create_user2 = True

        try:
            u3 = User.objects.get(username=regular_user_username)
        except User.DoesNotExist:
            should_create_user3 = True

        if should_create_user1:
            user_a1 = User(username=first_admin_username)
            user_a1.set_unusable_password()
            user_a1.is_staff = True
            user_a1.is_superuser = True
            user_a1.save()
            user_b1 = BlockchainUser(user=user_a1, human_friendly_name="Test Admin 1")
            user_b1.private_key = "0x6cbed15c793ce57650b9877cf6fa156fbef513c4e6134f022a85b1ffdd59b2a1"
            user_b1.save()

        if should_create_user2:
            user_a2 = User(username=second_admin_username)
            user_a2.set_unusable_password()
            user_a2.is_staff = True
            user_a2.is_superuser = True
            user_a2.save()
            user_b2 = BlockchainUser(user=user_a2, human_friendly_name="Test Admin 2 (authorizer)")
            user_b2.private_key = "0x6370fd033278c143179d81c5526140625662b8daa446c22ee2d73db3707e620c"
            user_b2.save()

        if should_create_user3:
            user_r1 = User(username=regular_user_username)
            user_r1.set_unusable_password()
            user_r1.is_staff = False
            user_r1.is_superuser = False
            user_r1.save()
            user_b3 = BlockchainUser(user=user_r1, human_friendly_name="Normal User")
            user_b3.private_key = "0x646f1ce2fdad0e6deeeb5c7e8e5543bdde65e86029e2fd9fc169899c440a7913"
            user_b3.save()
