import json

from django.core.management import BaseCommand
from django_ethereum_events.chainevents import AbstractEventReceiver
from django_ethereum_events.models import MonitoredEvent

from ...models import ServerBlockchainConfiguration, Commit, BlockchainUser, Token

config = ServerBlockchainConfiguration.load()


class ChangeCanBeStagedReceiver(AbstractEventReceiver):
    def save(self, decoded_event):
        print("--------------CHANGE CAN BE STAGED EVENT RECEIVED-------------")
        data = decoded_event["args"]
        commit_hash = data["_commitHash"]
        commit_creator = data["_creator"]
        print(f"{commit_creator} | {commit_hash}")
        try:
            creator = BlockchainUser.objects.get(user__username=commit_creator)
            commits_queryset = Commit.objects.filter(
                commit_hash=commit_hash, creator=creator
            )
        except BlockchainUser.DoesNotExist:
            print("ERROR: there is no BlockchainUser with that Public key")
            return

        if len(commits_queryset) != 1:
            print("ERROR: there is no Commit with that hash created by that BlockchainUser")
            return

        commit = commits_queryset[0]
        if commit.is_staged:
            print("ERROR: This commit is already staged")
            return

        if commit.is_commit_authorized:
            print("ERROR: This commit is already authorized")
            return

        commit.is_commit_authorized = True
        commit.save()


class TokenAuthenticatedReceiver(AbstractEventReceiver):
    def save(self, decoded_event):
        print("AUTHENTICATING TOKEN")
        data = decoded_event["args"]
        authenticated_challenge = data["_challenge"]
        authenticated_public_key = data["_address"]
        token = Token.objects.safe_get(challenge=authenticated_challenge)
        if not token:
            print("ERROR: There is no token with that challenge")

        if token.bc_user.user.username != authenticated_public_key:
            print("ERROR: This token is not associated with that user")

        token.is_authenticated = True
        token.save()
        print("TOKEN WAS AUTHENTICATED")


receiver = "poc_backend.management.commands.register_events.ChangeCanBeStagedReceiver"
receiver2 = "poc_backend.management.commands.register_events.TokenAuthenticatedReceiver"

DEFAULT_EVENTS = [
    (
        "ChangeCanBeStaged",
        config.contract_address,
        json.loads(config.contract_abi),
        receiver,
    ),
    (
        "TokenAuthenticated",
        config.contract_address,
        json.loads(config.contract_abi),
        receiver2,
    ),
]


class Command(BaseCommand):
    help = "Registers StageChangeEvent. This command should be run once, after contract is deployed"

    def handle(self, *args, **options):
        monitored_events = MonitoredEvent.objects.all()
        for event in DEFAULT_EVENTS:
            if not monitored_events.filter(
                name=event[0], contract_address__iexact=event[1]
            ).exists():
                self.stdout.write(f"Creating monitor for event {event[0]} at {event[1]}")
                MonitoredEvent.objects.register_event(*event)

        self.stdout.write(self.style.SUCCESS("Events are up to date"))
