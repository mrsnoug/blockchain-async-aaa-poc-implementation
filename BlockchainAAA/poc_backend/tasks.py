import json
from celery import shared_task

from .models import Commit, Configuration


@shared_task
def stage_commits():
    def implement_changes(changes_to_implement, server_config):
        server_config.string_data = changes_to_implement["string_data"]
        server_config.boolean_data = changes_to_implement["boolean_data"]
        server_config.number_data = changes_to_implement["number_data"]
        server_config.website_header_color = changes_to_implement["website_header_color"]
        server_config.save()
        print("STAGED A COMMIT")

    print("Trying to stage commits...")
    config = Configuration.load()
    authorized_commits = Commit.objects.filter(is_commit_authorized=True, is_staged=False)
    if len(authorized_commits) == 0:
        return

    for commit in authorized_commits:
        preceding_commits = commit.depends_on.all()
        changes = commit.changes
        if len(preceding_commits) == 0:
            implement_changes(changes, config)
            commit.is_staged = True
            commit.save()
        else:
            can_be_staged = True
            for pre_commit in preceding_commits:
                if not pre_commit.is_staged:
                    can_be_staged = False
                    break

            if can_be_staged:
                implement_changes(changes, config)
                commit.is_staged = True
                commit.save()
